
import java.util.ArrayList;

public class Postfix
{
    public double eval(String expr)
    {
        String[] tokens = expr.split(" ");
        return eval(tokens);
    }

    public double eval(ArrayList<String>tokens)
    {
        return eval((String[])tokens.toArray(new String[tokens.size()]));
    } 

    public double eval(String[] tokens)
    {
        Stack<Double> s = new Stack<Double>();

        for (String token:tokens)
        {
            switch(token)
            {
                case"pi":
                {
                    s.push(Math.PI);
                    break;
                }

                case "+":
                {

                    double a = s.pop();
                    double b = s.pop();
                    s.push(b + a);
                    break;
                }
                case "-":
                {
                    double a = s.pop();
                    double b = s.pop();
                    s.push(b - a);
                    break;
                }
                case "x":
                {
                    double a = s.pop();
                    double b = s.pop();
                    s.push(b * a);
                    break;
                }
                case "/":
                {
                    double a = s.pop();
                    double b = s.pop();
                    s.push(b / a);
                    break;
                }
                case "^":
                {
                    double a = s.pop();
                    double b = s.pop();
                    s.push(Math.pow(b, a));
                    break;
                }

                case "sin":
                {
                    double a = s.pop();
                    s.push(Math.sin(a));
                    break;
                }

                case "cos":
                {
                    double a = s.pop();
                    s.push(Math.cos(a));
                    break;
                }

                case "tan":
                {
                    double a = s.pop();
                    s.push(Math.tan(a));
                    break;
                }

                default:
                try
                {
                    s.push(Double.parseDouble(token));
                }

                catch (NumberFormatException nfe)
                {
                    throw new InvalidTokenException();
                }

                break;

            }
        }
        return s.peek();
    }

}
